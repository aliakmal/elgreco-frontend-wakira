import { ComboSelectable } from './combo_selectable.model'
import { Photo } from './photo.model'
export class ComboItem {
    name: string;
    id: number;
    description: string;
    base_price: number;
    menu_section_id: number;
    created_at: string;
    updated_at: string;
    photo_id: number;
    photo: Photo;
    selectables:Array<ComboSelectable>;
    constructor(combo){
        this.name = combo.name;
        this.id = combo.id;
        this.description = combo.description;
        this.base_price = combo.base_price;
        this.menu_section_id = combo.menu_section_id;
        this.created_at = combo.created_at;
        this.updated_at = combo.updated_at;
        this.photo_id = combo.photo_id;
        this.photo = new Photo(combo.photo);
        this.selectables = [];
        for(let i in combo.selectables){
            let selectable = new ComboSelectable(combo.selectables[i])
            this.selectables.push(selectable);
        }
    
    }

}

