import { Address } from "./address.model";
import { OrderItem } from "./order_item.model";
import { Basket } from "./basket.model";
import { CartItem } from "./cart_item.model";
import { CartComboItem } from "./cart_combo.model";
import { environment } from '../../environments/environment';
import { User } from "./user.model";

export class Order {

    id:number;
    ref:string;
    items:Array<OrderItem>;
    address:Address;

    zone_id:number;
    comments:string;
    customer_name:string;
    customer_email:string;
    customer_phone:string;

    payment_mode:string;
    payment_status:string;

    subtotal:number;
    delivery_charge:number;
    taxes:number;
    total:number;

    is_preorder:number;
    preorder_at:string;
    state:string;

    user_id:number;

    business_id:number;

    constructor(){ 
        this.state = 'new';
        this.payment_mode = 'cash';
        this.payment_status = null;
        this.items = [];

        this.address = new Address({});

        this.zone_id = null;
        this.comments = '';
        this.customer_name = '';
        this.customer_email = '';
        this.customer_phone = '';
        this.subtotal = 0;
        this.delivery_charge = 0;
        this.taxes = 0;
        this.total = 0;

        this.is_preorder = 0;
        this.preorder_at = null;
        this.ref = '_';
        this.user_id = null;
        this.business_id = environment.restaurantID;


    }

    isOnlinePayment(){
        return this.payment_mode == 'card';
    }

    initForOnlinePayment(){
        this.payment_mode = 'card';
        this.payment_status = 'pending';
    }
    
    initForOfflinePayment(){
        this.state = 'pending';
    }

    attachUser(user){
        this.user_id = user.id;
        this.customer_email = user.email;
        this.customer_name = user.name;
        this.customer_phone = user.phone;
    }

    attachBasket(basket:Basket){
        for(let i in basket.items){
            let item = basket.items[i] as CartItem;
            let order_item = new OrderItem(item);
            this.items.push(order_item);
        }

        for(let i in basket.combos){
            let combo = basket.combos[i] as CartComboItem;
            let order_item = new OrderItem(combo);
            this.items.push(order_item);
        }

        this.subtotal = basket.subtotal;
        this.delivery_charge = basket.delivery_charges;
        this.total = basket.total;
    }

    attachAddress(address:Address){
        this.address = address;
    }
}

