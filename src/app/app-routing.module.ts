import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './components/menu/menu.component';
import { LoginComponent } from './components/login/login.component';
import { CheckoutComponent } from './components/checkout/checkout.component'
import { RegisterComponent } from './components/register/register.component';
import { PayComponent } from './components/pay/pay.component';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { HistoryComponent } from './components/history/history.component';

const routes: Routes = [
  { path: '', component: MenuComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'pay', component: PayComponent },
  { path: 'cancelled', component: PayComponent },
  { path: 'error', component: PayComponent },
  { path: 'confirm', component: ConfirmComponent },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
