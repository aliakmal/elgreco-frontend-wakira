import { SlugifyPipe } from '../shared/slugify.pipe'
import { ItemVariation } from './item_variation.model'
import { Item } from './item.model'
import { Addon } from './addon.model'
import { Choice } from './choice.model'
import { SelectOption } from './select_option.model'
export class CartItem {
    id: string;
    name: string;
    description: string;
    item: Item;
    selected_variation:ItemVariation;
    selected_addons:Array<SelectOption>;
    selected_choices:Array<SelectOption>;
    unit_price: number;
    subtotal_price: number;
    qty: number;
    type:string;
    formatted_extras:string;

    private slugifyPipe;


    initFromBasketItem(cart_item:CartItem){
        this.item = new Item(cart_item.item);
        this.name = cart_item.name;
        this.description = cart_item.description;
        this.slugifyPipe = new SlugifyPipe();
        this.qty = cart_item.qty;
        this.type = 'item';
        this.selected_variation = null;
        this.selected_addons = [];
        this.selected_choices = [];

        for(let i in cart_item.selected_addons){
            let option = new SelectOption();
            option.initFromOption(cart_item.selected_addons[i]);
            this.selected_addons.push(option);
        }

        for(let i in cart_item.selected_choices){
            let option = new SelectOption();
            option.initFromOption(cart_item.selected_choices[i]);
            this.selected_choices.push(option);
        }

        if(this.item.hasVariations()){
            let _selected_variation = new ItemVariation(cart_item.selected_variation);
            this.selected_variation = _selected_variation;
            this.selected_variation.selected_options = [];

            for(let i in cart_item.selected_variation.selected_options){
                let selected_option = new SelectOption();
                selected_option.initFromOption(cart_item.selected_variation.selected_options[i])
                this.selected_variation.toggleOption(selected_option);
            }

        }

        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();

    }

    refreshAll(){
        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();
    }

    selectFirstVariation(){
        if(this.item.hasVariations()){
            this.selectVariation(this.item.variations[0]);
        }else{
            return;
        }

        this.selectInitialOptionsForSelectedVariation();
        this.refreshAll();
    }






    initFromItem(item:Item){
        this.item = item;
        this.name = item.name;
        this.description = item.description;
        this.slugifyPipe = new SlugifyPipe();
        this.qty = 1;
        this.type = 'item';
        this.selected_addons = [];
        this.selected_choices = [];


        if(this.item.hasVariations()){
            this.selectVariation(this.item.getFirstVariation());
        }else{
            this.selected_variation = null;
            this.updateLocalId();
            this.updatePrices();
            this.updateFormattedExtras();
        }

    }

    isSelectedAddon(option){
        for(let i in this.selected_addons){
            if(option.option_of == this.selected_addons[i].option_of){
                if(this.selected_addons[i].name == option.name){
                    return true;
                }
            }
        }

        return false;
    }

    isSelectedChoice(option){
        for(let i in this.selected_choices){
            if(option.option_of == this.selected_choices[i].option_of){
                if(this.selected_choices[i].name == option.name){
                    return true;
                }
            }
        }

        return false;
    }


    getType(){
        return 'item';
    }

    getFormattedExtras(){
        let extras = '';
        if(this.selected_variation){
            extras = this.selected_variation.getFormattedExtras();
        }else{
            extras = this.selected_choices.map(res=>{ return res.option_of+': '+res.name+''}).join(', ');
        }
        return extras;
    }

    initializeSelectedVariation(variation, idx){
        if(idx === 0){
            this.selectVariation(variation);
            return true;
        }
        return false;
    }

    toggleChoice(option){
        let choice_selected = false;
        let selected_option = new SelectOption();
        selected_option.initFromOption(option);
        for(let i in this.selected_choices){
            if(option.option_of == this.selected_choices[i].option_of){
                this.selected_choices[i] = selected_option;
                choice_selected = true;
            }
        }

        if(choice_selected == false){
            this.selected_choices.push(selected_option);
        }
        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();
        
    }

    decrement(num){
        this.qty = this.qty <=1 ? 1 : (this.qty - 1);
        this.updatePrices();
    }

    increment(num){
        this.qty = this.qty + num;
        this.updatePrices();
    }

    selectVariationOption(variation, option){

        variation.toggleOption(option);
        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();

    }

    selectSelectedVariationOption(option){

        this.selected_variation.toggleOption(option);
        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();

    }
    selectInitialOptionsForSelectedVariation(){
        this.selected_variation.selected_options = [];
        for(let i in this.selected_variation.choices){
            this.selected_variation.selected_options.push(this.selected_variation.choices[i].selectable_options[0]);
        }
    }
    selectVariation(variation){

        this.selected_variation = new ItemVariation(JSON.parse(JSON.stringify(variation)));
        this.selected_variation.resetSelectedOptions();
        this.selectInitialOptionsForSelectedVariation();
        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();
    }

    isSelectedVariation(variation){
        return this.selected_variation.name == variation['name'] ? true : false;
    }

    updateFormattedExtras(){
        this.formatted_extras = this.getFormattedExtras();
    }

    updatePrices(){
        this.updateUnitPrice();
        this.updateSubTotalPrice();
    }

    updateLocalId(){
        let extras = (this.selected_variation?this.selected_variation.getFormattedExtras():this.getFormattedExtras());
        this.id = this.slugifyPipe.transform(this.item.name+' '+extras);
    }

    updateUnitPrice(){
        this.unit_price = this.getItemPrice()
    }

    updateSubTotalPrice(){
        this.subtotal_price = this.unit_price*this.qty;
    }



    getItemPrice(){
        let price = 0;
        if(this.item.variations.length == 0){
            price+= +this.item.base_price;
            for(let i in this.selected_choices){
                price+=+this.selected_choices[i].price;
            }
        }else{
            price+= +this.selected_variation.getPrices();
        }

        return price;
    }

}

