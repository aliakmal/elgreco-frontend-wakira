import { Choice } from './choice.model'
import { SelectOption } from './select_option.model'

export class ItemVariation {
    name: string;
    id: number;
    description: string;
    price: number;
    choices:Array<any>;
    selectable_choices:Array<any>;
    selected_options:Array<SelectOption>;

    formatted_name:string;
    unit_price:number;
    constructor(variation){
        this.name = variation.name;
        this.id = variation.id;
        this.description = variation.description;
        this.price = variation.price;
        this.choices = [];
        this.selected_options = [];
        this.formatted_name = '';
        this.unit_price = this.price;
        for(let i in variation.choices){
            let choice = new Choice(variation.choices[i]);
            this.choices.push(choice);
        }

        this.updatePrices();
        this.updateFormattedExtras();

    }

    isSelectedOption(option){
        for(let i in this.selected_options){
            if(option.option_of == this.selected_options[i].option_of){
                if(this.selected_options[i].name == option.name){
                    return true;
                }
            }
        }

        return false;
    }

    resetSelectedOptions(){
        this.selected_options = [];
    }

    toggleOption(option){
        let selected_option = new SelectOption();
        selected_option.initFromOption(option);

        let option_found = false;
        for(let i in this.selected_options){
            if(selected_option.option_of == this.selected_options[i].option_of){
                this.selected_options[i] = selected_option;
                option_found = true;
            }
        }

        if(option_found == false){
            this.selected_options.push(selected_option);
        }

        this.updatePrices();
        this.updateFormattedExtras();
    }

    updatePrices(){
        this.unit_price = this.getPrices();
    }
    getPrices(){
        let price = +this.price;
        if(this.selected_options.length > 0){
            for(let i in this.selected_options){
                price = +this.selected_options[i].price + price;
            }
        }

        return +price;
    }

    updateFormattedExtras(){
        this.formatted_name = this.getFormattedExtras();

    }

    getFormattedExtras(){
        let extras = this.selected_options.map(res=>{ return res.option_of+': '+res.name+''}).join(', ');
        if(extras != ''){
            extras = '('+extras+')';
        }
        return this.name+extras;
    }





}

